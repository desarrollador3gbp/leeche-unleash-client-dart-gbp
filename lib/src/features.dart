/// See https://unleash.github.io/docs/api/client/features
class Features {
  Features({this.version, this.features});

  factory Features.fromJson(Map<String, dynamic> json) {
    var features = <FeatureToggle>[];
    if (json['enable'] != null) {
      json['enable'].forEach((dynamic v) {
        features.add(FeatureToggle(enabled: true, name: v as String));
      });
    }

    if (json['fullDisabled'] != null) {
      json['fullDisabled'].forEach((dynamic v) {
        features.add(FeatureToggle(enabled: false, name: v as String));
      });
    }

    if (json['disabledText'] != null) {
      json['disabledText'].forEach((dynamic v) {
        features.add(FeatureToggle.fromJson(v as Map<String, dynamic>)
            .copyWith(enabled: false));
      });
    }

    if (json['features'] != null) {
      json['features'].forEach((dynamic v) {
        features.add(FeatureToggle.fromJson(v as Map<String, dynamic>));
      });
    }

    return Features(
      version: json['version'] as int?,
      features: features,
    );
  }

  final int? version;
  final List<FeatureToggle>? features;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      if (version != null) 'version': version,
      if (features?.isNotEmpty ?? false)
        'features': features?.map((e) => e.toJson()).toList(),
    };
  }
}

class FeatureToggle {
  FeatureToggle({
    this.name,
    this.description,
    this.enabled,
    this.strategies,
    this.strategy,
  });

  FeatureToggle copyWith({
    String? name,
    String? description,
    bool? enabled,
    List<Strategy>? strategies,
    String? strategy,
  }) {
    return FeatureToggle(
      name: name ?? this.name,
      description: description ?? this.description,
      enabled: enabled ?? this.enabled,
      strategies: strategies ?? this.strategies,
      strategy: strategy ?? this.strategy,
    );
  }

  factory FeatureToggle.fromJson(Map<String, dynamic> json) {
    var strategies = <Strategy>[];
    if (json['strategies'] != null) {
      json['strategies'].forEach((dynamic v) {
        strategies.add(Strategy.fromJson(v as Map<String, dynamic>));
      });
    }

    return FeatureToggle(
      name: (json['flag'] ?? json['name']) as String?,
      description: (json['texto'] ?? json['description']) as String?,
      enabled: (json['enabled'] ?? json['enabled']) as bool?,
      strategies: strategies,
      strategy: (json['strategy'] ?? json['strategy']) as String?,
    );
  }

  final String? name;
  final String? description;
  final bool? enabled;
  final List<Strategy>? strategies;
  final String? strategy;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      if (name != null) 'name': name,
      if (description != null) 'description': description,
      if (enabled != null) 'enabled': enabled,
      if (strategy != null) 'strategy': strategy,
      if (strategies?.isNotEmpty ?? false)
        'strategies': strategies?.map((e) => e.toJson()).toList(),
    };
  }
}

class Strategy {
  Strategy({this.name, this.parameters});

  factory Strategy.fromJson(Map<String, dynamic> json) {
    return Strategy(
      name: json['name'] as String?,
      parameters: json['parameters'] != null
          ? json['parameters'] as Map<String, dynamic>?
          : null,
    );
  }

  final String? name;
  final Map<String, dynamic>? parameters;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      if (name != null) 'name': name,
      if (parameters?.isNotEmpty ?? true) 'parameters': parameters,
    };
  }
}
